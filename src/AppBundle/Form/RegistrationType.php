<?php
/**
 * Created by Shamray.

 */

namespace AppBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class RegistrationType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('resident', 'choice', array(
        'choices' => array(
          1 => 'Отправитель резидент РБ',
          2 => 'Отправитель резидент РФ',
          3 => 'Отправитель резидент РФ'
        ),
        'expanded' => true,
        'multiple' => false,
        'label_format' => 'form.resident',
        'translation_domain' => 'FOSUserBundle',
      ))
      ->add('organization', null, array('label' => 'form.organization', 'translation_domain' => 'FOSUserBundle'))
      ->add('username', null, array('label' => 'form.person', 'translation_domain' => 'FOSUserBundle'))
      ->add('phone', 'text', array('label' => 'form.phone', 'translation_domain' => 'FOSUserBundle'))
      ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
      ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
        'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
        'options' => array('translation_domain' => 'FOSUserBundle'),
        'first_options' => array('label' => 'form.password'),
        'second_options' => array('label' => 'form.password_confirmation'),
        'invalid_message' => 'fos_user.password.mismatch',
      ));
  }

  public function getParent()
  {
    return 'FOS\UserBundle\Form\Type\RegistrationFormType';
  }

  public function getBlockPrefix()
  {
    return 'app_user_registration';
  }

  public function getName()
  {
    return $this->getBlockPrefix();
  }

}