<?php
/**
 * Override FOS\UserBundle\EventListener\EmailConfirmationListener;
 */

namespace AppBundle\EventListener;

use FOS\UserBundle\EventListener\EmailConfirmationListener as BaseListener;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class EmailConfirmationListener extends BaseListener
{

  private $mailer;

  private $tokenGenerator;

  private $session;

  private $templating;

  private $container;

  public function __construct($container, $mailer, TokenGeneratorInterface $tokenGenerator, UrlGeneratorInterface $router, SessionInterface $session, EngineInterface $templating)
  {
    $this->mailer = $mailer;
    $this->tokenGenerator = $tokenGenerator;
    $this->doctrine = $router;
    $this->session = $session;
    $this->templating = $templating;
    $this->container = $container;
  }

  public function onRegistrationSuccess(FormEvent $event)
  {
    $user = $event->getForm()->getData();
    $user->setEnabled(false);
    if (null === $user->getConfirmationToken()) {
      $user->setConfirmationToken($this->tokenGenerator->generateToken());
    }
    $email = $user->getEmail();
    $nameResident = $this->getResidentName($user->getResident());
    $userName = $user->getUsername();
    $subject = $this->container->get('translator')->trans('registration.email.subject', array('%username%' => $userName), 'FOSUserBundle');
    $maierlUser = $this->container->getParameter('mailer_user');

    $message = \Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom($maierlUser)
      ->setTo($email)
      ->setBody(
        $this->templating->render('Emails/loggedin.html.twig',
          array('name' => $userName, 'resident' => $nameResident)),
        'text/html'
      )
      ->attach(\Swift_Attachment::fromPath($this->container->getParameter('treaty_directory') . '/' . $user->getResident() . '/' . 'document1.doc'))
      ->attach(\Swift_Attachment::fromPath($this->container->getParameter('treaty_directory') . '/' . $user->getResident() . '/' . 'document2.docx'))
      ->attach(\Swift_Attachment::fromPath($this->container->getParameter('treaty_directory') . '/' . $user->getResident() . '/' . 'document3.docx'));

    $this->mailer->send($message);
    $this->session->set('fos_user_send_confirmation_email/email', $user->getEmail());
    $url = $this->container->get('router')->generate('auth_notification');
    $event->setResponse(new RedirectResponse($url));
  }


  /**
   * @param int $choose
   * @return string $resident
   */
  private function getResidentName($choose)
  {

    switch ($choose) {

      case 1:
        $resident = "Отправитель резидент РБ";
        break;

      case 2:
        $resident = "Отправитель резидент РФ";
        break;

      case 3:
        $resident = "Пполучатель резидент РФ";
        break;

      default:
        $resident = "";
        break;

    }
    return $resident;
  }

}