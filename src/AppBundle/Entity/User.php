<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{

  /**
   * One User has Many Deliveryorders.
   * @ORM\OneToMany(targetEntity="\Sota\DeliveryBundle\Entity\Delivery\Deliveryorder", mappedBy="user")
   */
  private $deliveryorder;

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  public function __construct()
  {
    $this->deliveryorder = new ArrayCollection();
    parent::__construct();
  }

  /**
   * @ORM\Column(type="integer", name="resident", nullable=false)
   */
  protected $resident;

  /**
   * @ORM\Column(type="string", name="person", length=255)
   * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
   * @Assert\Length(
   *     min=3,
   *     max=255,
   *     minMessage="The name person is too short.",
   *     maxMessage="The name person is too long.",
   *     groups={"Registration", "Profile"}
   * )
   */

  protected $organization;

  /**
   * @ORM\Column(type="integer", name="phone", nullable=false)
   */
  protected $phone;


  public function getResident()
  {
    return $this->resident;
  }

  public function setResident($resident)
  {
    $this->resident = $resident;
  }

  public function getOrganization()
  {
    return $this->organization;
  }

  public function setOrganization($organization)
  {
    $this->organization = $organization;
  }

  public function getPhone()
  {
    return $this->phone;
  }

  public function setPhone($phone)
  {
    $this->phone = $phone;
  }
}
