<?php
/**
 *
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{

  public function load(ObjectManager $manager)
  {

    // Create a new user
    $user = new User();;
    $user->setUsername('testUser');
    $user->setEmail('testUser@gmail.com');
    $user->setPlainPassword('1111');
    $user->setResident(2);
    $user->setOrganization('nameOrganization');
    $user->setPhone('0951234567');
    $user->setEnabled(true);
    $this->addReference('user', $user);

    $manager->persist($user);
    $manager->flush();

  }

  public function getOrder()
  {
    return 1;
  }
}
