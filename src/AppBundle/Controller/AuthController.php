<?php
/**
 *
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class AuthController extends Controller
{

  public function userAction()
  {
    return $this->render('user/login.html.twig');
  }

  /**
   * the notification after success of registration
   */
  public function notificationAction(Request $request)
  {
    $session = $request->getSession();
    $email = $session->get('fos_user_send_confirmation_email/email');
    $userName = $this->get('fos_user.user_manager')->findUserByEmail($email)->getUsername();

    return $this->render('user/notification.html.twig', array('userName' => $userName, 'email' => $email));
  }

}