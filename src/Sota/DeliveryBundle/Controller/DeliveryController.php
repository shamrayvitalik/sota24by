<?php
/**
 *
 */

namespace Sota\DeliveryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sota\DeliveryBundle\Entity\Delivery\City;
use Sota\DeliveryBundle\Entity\Delivery\Transport;
use Sota\DeliveryBundle\Entity\Delivery\Dispatchshedule;
use Sota\DeliveryBundle\Entity\Delivery\Producttype;
use Sota\DeliveryBundle\Entity\Delivery\Pricedeliveryincity;
use Sota\DeliveryBundle\Entity\Delivery\Priceofdelivery;
use Sota\DeliveryBundle\Entity\Delivery\Deliveryorder;
use Sota\DeliveryBundle\Entity\Delivery\Orderstatus;
use AppBundle\Entity\User;


class DeliveryController extends Controller
{

  public function indexAction()
  {
    if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {

      return $this->render('SotaDeliveryBundle:Default:index.html.twig');
    }
    return $this->render('user/login.html.twig');
  }


}