<?php

namespace Sota\DeliveryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     *
     */
    public function indexAction()
    {
        return $this->render('SotaDeliveryBundle:Default:index.html.twig');
    }
}
