<?php

namespace Sota\DeliveryBundle\Entity\Delivery;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * City
 * @ORM\Table(name="delivery_city")
 * @ORM\Entity(repositoryClass="Sota\DeliveryBundle\Repository\Delivery\CityRepository")
 */
class City
{

  /**
   * One City has Many Deliveryorders.
   * @ORM\OneToMany(targetEntity="Deliveryorder", mappedBy="city")
   */
  private $deliveryorder;

  /**
   * One City has Many Priceofdeliveries.
   * @ORM\OneToMany(targetEntity="Priceofdelivery", mappedBy="city")
   */
  private $priceofdelivery;

  /**
   * One City has Many Dispatchshedules.
   * @ORM\OneToMany(targetEntity="Dispatchshedule", mappedBy="city")
   */
  private $dispatchshedules;

  /**
   * One City has Many Pricedeliveryincities.
   * @ORM\OneToMany(targetEntity="Pricedeliveryincity", mappedBy="city")
   */
  private $pricedeliveryincities;

  /**
   * @var int
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   * @ORM\Column(name="name", type="string", length=45)
   */
  private $name;


  public function __construct()
  {
    $this->dispatchshedules = new ArrayCollection();
    $this->pricedeliveryincities = new ArrayCollection();
    $this->priceofdelivery = new ArrayCollection();
    $this->deliveryorder = new ArrayCollection();
  }

  /**
   * Get id
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set name
   * @param string $name
   * @return City
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * Get name
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }
}

