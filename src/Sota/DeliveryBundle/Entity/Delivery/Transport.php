<?php

namespace Sota\DeliveryBundle\Entity\Delivery;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Transport
 * @ORM\Table(name="delivery_transport")
 * @ORM\Entity(repositoryClass="Sota\DeliveryBundle\Repository\Delivery\TransportRepository")
 */
class Transport
{

  /**
   * One Transport has Many Deliveryorders.
   * @ORM\OneToMany(targetEntity="Deliveryorder", mappedBy="transport")
   */
  private $deliveryorder;

  /**
   * One Transport has Many Priceofdeliveries.
   * @ORM\OneToMany(targetEntity="Priceofdelivery", mappedBy="transport")
   */
  private $priceofdelivery;

  /**
   * One Transport has Many Dispatchshedules.
   * @ORM\OneToMany(targetEntity="Dispatchshedule", mappedBy="transport")
   */
  private $dispatchshedules;

  /**
   * @var int
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Producttype", inversedBy="transports")
   * @ORM\JoinColumn(name="productstype_id", referencedColumnName="id", nullable=false)
   */
  private $productstypeId;

  /**
   * @var string
   * @ORM\Column(name="name", type="string", length=45)
   */
  private $name;


  public function __construct()
  {
    $this->dispatchshedules = new ArrayCollection();
    $this->priceofdelivery = new ArrayCollection();
    $this->deliveryorder = new ArrayCollection();
  }

  /**
   * Get id
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set productstypeId
   * @param \Sota\DeliveryBundle\Entity\Delivery\Producttype $productstype
   * @return Transport
   */
  public function setProductstype($productstype)
  {
    $this->productstypeId = $productstype;

    return $this;
  }

  /**
   * Get productstypeId
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getProductstype()
  {
    return $this->productstypeId;
  }

  /**
   * Set name
   * @param string $name
   * @return Transport
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * Get name
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }
}

