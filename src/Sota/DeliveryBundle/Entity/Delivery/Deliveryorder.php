<?php

namespace Sota\DeliveryBundle\Entity\Delivery;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\User;

/**
 * Deliveryorder
 * @ORM\Table(name="delivery_deliveryorder")
 * @ORM\Entity(repositoryClass="Sota\DeliveryBundle\Repository\Delivery\DeliveryorderRepository")
 */
class Deliveryorder
{
  /**
   * @var int
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   * @ORM\Column(name="orderid", type="string", length=45, unique=true)
   */
  private $orderid;

  /**
   * @var \DateTime
   * @ORM\Column(name="dateofcreations", type="datetime")
   */
  private $dateofcreations;

  /**
   * @var float
   * @ORM\Column(name="cost", type="float")
   */
  private $cost;

  /**
   * @ORM\ManyToOne(targetEntity="City", inversedBy="deliveryorders")
   * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
   */
  private $cityId;

  /**
   * @ORM\ManyToOne(targetEntity="Transport", inversedBy="deliveryorders")
   * @ORM\JoinColumn(name="transport_id", referencedColumnName="id", nullable=false)
   */
  private $transportId;

  /**
   * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="deliveryorders")
   * @ORM\JoinColumn(name="user_iduser", referencedColumnName="id", nullable=false)
   */
  private $user;

  /**
   * @var float
   * @ORM\Column(name="weight", type="float")
   */
  private $weight;

  /**
   * @var int
   * @ORM\Column(name="countofpalets", type="integer")
   */
  private $countofpalets;

  /**
   * @var int
   * @ORM\Column(name="orderstatus", type="integer")
   */
  private $orderstatus;

  /**
   * @ORM\ManyToOne(targetEntity="Orderstatus", inversedBy="deliveryorders")
   * @ORM\JoinColumn(name="orderstatus_id", referencedColumnName="id", nullable=false)
   */
  private $orderstatusId;

  /**
   * Get id
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set orderid
   * @param string $orderid
   * @return Deliveryorder
   */
  public function setOrderid($orderid)
  {
    $this->orderid = $orderid;

    return $this;
  }

  /**
   * Get orderid
   * @return string
   */
  public function getOrderid()
  {
    return $this->orderid;
  }

  /**
   * Set dateofcreations
   * @param \DateTime $dateofcreations
   * @return Deliveryorder
   */
  public function setDateofcreations($dateofcreations)
  {
    $this->dateofcreations = $dateofcreations;

    return $this;
  }

  /**
   * Get dateofcreations
   * @return \DateTime
   */
  public function getDateofcreations()
  {
    return $this->dateofcreations;
  }

  /**
   * Set cost
   * @param float $cost
   * @return Deliveryorder
   */
  public function setCost($cost)
  {
    $this->cost = $cost;

    return $this;
  }

  /**
   * Get cost
   * @return float
   */
  public function getCost()
  {
    return $this->cost;
  }

  /**
   * Set cityId
   * @param \Sota\DeliveryBundle\Entity\Delivery\City $city
   * @return Deliveryorder
   */
  public function setCity($city)
  {
    $this->cityId = $city;

    return $this;
  }

  /**
   * Get cityId
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getCity()
  {
    return $this->cityId;
  }

  /**
   * Set transportId
   * @param \Sota\DeliveryBundle\Entity\Delivery\Transport $transport
   * @return Deliveryorder
   */
  public function setTransport($transport)
  {
    $this->transportId = $transport;

    return $this;
  }

  /**
   * Get transportId
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getTransport()
  {
    return $this->transportId;
  }

  /**
   * Set weight
   * @param float $weight
   * @return Deliveryorder
   */
  public function setWeight($weight)
  {
    $this->weight = $weight;

    return $this;
  }

  /**
   * Get weight
   * @return float
   */
  public function getWeight()
  {
    return $this->weight;
  }

  /**
   * Set countofpalets
   * @param integer $countofpalets
   * @return Deliveryorder
   */
  public function setCountofpalets($countofpalets)
  {
    $this->countofpalets = $countofpalets;

    return $this;
  }

  /**
   * Get countofpalets
   * @return int
   */
  public function getCountofpalets()
  {
    return $this->countofpalets;
  }

  /**
   * Set orderstatus
   * @param integer $orderstatus
   * @return Deliveryorder
   */
  public function setOrderstatus($orderstatus)
  {
    $this->orderstatus = $orderstatus;

    return $this;
  }

  /**
   * Get orderstatus
   * @return int
   */
  public function getOrderstatus()
  {
    return $this->orderstatus;
  }

  /**
   * Set orderstatusId
   * @param \Sota\DeliveryBundle\Entity\Delivery\Orderstatus $orderstatusId
   * @return Deliveryorder
   */
  public function setOrderstatusId($orderstatusId)
  {
    $this->orderstatusId = $orderstatusId;

    return $this;
  }

  /**
   * Get orderstatusId
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getOrderstatusId()
  {
    return $this->orderstatusId;
  }

  /**
   * Set user
   * @param \AppBundle\Entity\User $user
   * @return Deliveryorder
   */
  public function setUser(\AppBundle\Entity\User $user = null)
  {
    $this->user = $user;

    return $this;
  }

  /**
   * Get user
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getUser()
  {
    return $this->user;
  }
}

