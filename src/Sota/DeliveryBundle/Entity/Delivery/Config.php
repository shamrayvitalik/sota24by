<?php

namespace Sota\DeliveryBundle\Entity\Delivery;

use Doctrine\ORM\Mapping as ORM;

/**
 * Config
 * @ORM\Table(name="delivery_config")
 * @ORM\Entity(repositoryClass="Sota\DeliveryBundle\Repository\Delivery\ConfigRepository")
 */
class Config
{
  /**
   * @var int
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   * @ORM\Column(name="clef", type="string", length=45, unique=true)
   */
  private $clef;

  /**
   * @var string
   * @ORM\Column(name="value", type="string", length=45)
   */
  private $value;


  /**
   * Get id
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set clef
   * @param string $clef
   * @return Config
   */
  public function setClef($clef)
  {
    $this->clef = $clef;

    return $this;
  }

  /**
   * Get clef
   * @return string
   */
  public function getClef()
  {
    return $this->clef;
  }

  /**
   * Set value
   * @param string $value
   * @return Config
   */
  public function setValue($value)
  {
    $this->value = $value;

    return $this;
  }

  /**
   * Get value
   * @return string
   */
  public function getValue()
  {
    return $this->value;
  }
}

