<?php

namespace Sota\DeliveryBundle\Entity\Delivery;

use Doctrine\ORM\Mapping as ORM;

/**
 * Priceofdelivery
 * @ORM\Table(name="delivery_priceofdelivery")
 * @ORM\Entity(repositoryClass="Sota\DeliveryBundle\Repository\Delivery\PriceofdeliveryRepository")
 */
class Priceofdelivery
{
  /**
   * @var int
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="City", inversedBy="priceofdeliveries")
   * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
   */
  private $cityId;

  /**
   * @ORM\ManyToOne(targetEntity="Transport", inversedBy="priceofdeliveries")
   * @ORM\JoinColumn(name="transport_id", referencedColumnName="id", nullable=false)
   */
  private $transportId;

  /**
   * @var int
   * @ORM\Column(name="countdeliverydays", type="integer")
   */
  private $countdeliverydays;

  /**
   * @var float
   * @ORM\Column(name="deliveryprice", type="float")
   */
  private $deliveryprice;


  /**
   * Get id
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set cityId
   * @param \Sota\DeliveryBundle\Entity\Delivery\City $city
   * @return Priceofdelivery
   */
  public function setCity($city)
  {
    $this->cityId = $city;

    return $this;
  }

  /**
   * Get cityId
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getCity()
  {
    return $this->cityId;
  }

  /**
   * Set transportId
   * @param \Sota\DeliveryBundle\Entity\Delivery\Transport $transport
   * @return Priceofdelivery
   */
  public function setTransport($transport)
  {
    $this->transportId = $transport;

    return $this;
  }

  /**
   * Get transportId
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getTransport()
  {
    return $this->transportId;
  }

  /**
   * Set countdeliverydays
   * @param integer $countdeliverydays
   * @return Priceofdelivery
   */
  public function setCountdeliverydays($countdeliverydays)
  {
    $this->countdeliverydays = $countdeliverydays;

    return $this;
  }

  /**
   * Get countdeliverydays
   * @return int
   */
  public function getCountdeliverydays()
  {
    return $this->countdeliverydays;
  }

  /**
   * Set deliveryprice
   * @param float $deliveryprice
   * @return Priceofdelivery
   */
  public function setDeliveryprice($deliveryprice)
  {
    $this->deliveryprice = $deliveryprice;

    return $this;
  }

  /**
   * Get deliveryprice
   * @return float
   */
  public function getDeliveryprice()
  {
    return $this->deliveryprice;
  }
}

