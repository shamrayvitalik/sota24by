<?php

namespace Sota\DeliveryBundle\Entity\Delivery;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Producttype
 * @ORM\Table(name="delivery_producttype")
 * @ORM\Entity(repositoryClass="Sota\DeliveryBundle\Repository\Delivery\ProducttypeRepository")
 */
class Producttype
{

  /**
   * One Producttype has Many Pricedeliveryincities.
   * @ORM\OneToMany(targetEntity="Pricedeliveryincity", mappedBy="producttype")
   */
  private $pricedeliveryincities;

  /**
   * One Producttype has Many transports.
   * @ORM\OneToMany(targetEntity="Transport", mappedBy="producttype")
   */
  private $transports;

  /**
   * @var int
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   * @ORM\Column(name="name", type="string", length=45)
   */
  private $name;

  /**
   * @var string
   * @ORM\Column(name="description", type="text", nullable=true)
   */
  private $description;


  public function __construct()
  {
    $this->pricedeliveryincities = new ArrayCollection();
    $this->transports = new ArrayCollection();
  }

  /**
   * Get id
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set name
   * @param string $name
   * @return Producttype
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * Get name
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set description
   * @param string $description
   * @return Producttype
   */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * Get description
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }
}

