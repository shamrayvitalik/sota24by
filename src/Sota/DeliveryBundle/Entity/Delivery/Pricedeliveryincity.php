<?php

namespace Sota\DeliveryBundle\Entity\Delivery;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pricedeliveryincity
 * @ORM\Table(name="delivery_pricedeliveryincity")
 * @ORM\Entity(repositoryClass="Sota\DeliveryBundle\Repository\Delivery\PricedeliveryincityRepository")
 */
class Pricedeliveryincity
{

  /**
   * @var int
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="City", inversedBy="pricedeliveryincities")
   * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
   */
  private $cityId;

  /**
   * @ORM\ManyToOne(targetEntity="Producttype", inversedBy="pricedeliveryincities")
   * @ORM\JoinColumn(name="productstype_id", referencedColumnName="id", nullable=false)
   */
  private $productstypeId;

  /**
   * @var string
   * @ORM\Column(name="discription", type="text", nullable=true)
   */
  private $discription;

  /**
   * @var float
   * @ORM\Column(name="pricedeliverycity", type="float")
   */
  private $pricedeliverycity;


  /**
   * Get id
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set cityId
   * @param \Sota\DeliveryBundle\Entity\Delivery\City $city
   * @return Pricedeliveryincity
   */
  public function setCity($city)
  {
    $this->cityId = $city;

    return $this;
  }

  /**
   * Get cityId
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getCity()
  {
    return $this->cityId;
  }

  /**
   * Set productstypeId
   * @param \Sota\DeliveryBundle\Entity\Delivery\Producttype $productstype
   * @return Pricedeliveryincity
   */
  public function setProductstype($productstype)
  {
    $this->productstypeId = $productstype;

    return $this;
  }

  /**
   * Get productstypeId
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getProductstype()
  {
    return $this->productstypeId;
  }

  /**
   * Set discription
   * @param string $discription
   * @return Pricedeliveryincity
   */
  public function setDiscription($discription)
  {
    $this->discription = $discription;

    return $this;
  }

  /**
   * Get discription
   * @return string
   */
  public function getDiscription()
  {
    return $this->discription;
  }

  /**
   * Set pricedeliverycity
   * @param float $pricedeliverycity
   * @return Pricedeliveryincity
   */
  public function setPricedeliverycity($pricedeliverycity)
  {
    $this->pricedeliverycity = $pricedeliverycity;

    return $this;
  }

  /**
   * Get pricedeliverycity
   * @return float
   */
  public function getPricedeliverycity()
  {
    return $this->pricedeliverycity;
  }
}

