<?php

namespace Sota\DeliveryBundle\Entity\Delivery;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dispatchshedule
 * @ORM\Table(name="delivery_dispatchshedule")
 * @ORM\Entity(repositoryClass="Sota\DeliveryBundle\Repository\Delivery\DispatchsheduleRepository")
 */
class Dispatchshedule
{

  /**
   * @var int
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="City", inversedBy="dispatchshedules")
   * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
   */
  private $cityId;

  /**
   * @ORM\ManyToOne(targetEntity="Transport", inversedBy="dispatchshedules")
   * @ORM\JoinColumn(name="transport_id", referencedColumnName="id", nullable=false)
   */
  private $transportId;

  /**
   * @var \DateTime
   * @ORM\Column(name="dayofdispatche", type="datetime")
   */
  private $dayofdispatche;

  /**
   * @var string
   * @ORM\Column(name="description", type="string", length=45, nullable=true)
   */
  private $description;


  /**
   * Get id
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set cityId
   * @param \Sota\DeliveryBundle\Entity\Delivery\City $city
   * @return Dispatchshedule
   */
  public function setCity($city)
  {
    $this->cityId = $city;

    return $this;
  }

  /**
   * Get cityId
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getCity()
  {
    return $this->cityId;
  }

  /**
   * Set transportId
   * @param \Sota\DeliveryBundle\Entity\Delivery\Transport $transport
   * @return Dispatchshedule
   */
  public function setTransport($transport)
  {
    $this->transportId = $transport;

    return $this;
  }

  /**
   * Get transportId
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getTransport()
  {
    return $this->transportId;
  }

  /**
   * Set dayofdispatche
   * @param \DateTime $dayofdispatche
   * @return Dispatchshedule
   */
  public function setDayofdispatche($dayofdispatche)
  {
    $this->dayofdispatche = $dayofdispatche;

    return $this;
  }

  /**
   * Get dayofdispatche
   * @return \DateTime
   */
  public function getDayofdispatche()
  {
    return $this->dayofdispatche;
  }

  /**
   * Set description
   * @param string $description
   * @return Dispatchshedule
   */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * Get description
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

}

