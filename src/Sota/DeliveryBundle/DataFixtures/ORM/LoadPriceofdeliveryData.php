<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.04.17
 * Time: 17:50
 */

namespace Sota\DeliveryBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sota\DeliveryBundle\Entity\Delivery\Priceofdelivery;


class LoadPriceofdeliveryData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $priceofdelivery = new Priceofdelivery();
    $priceofdelivery->setCity($this->getReference('city'));
    $priceofdelivery->setTransport($this->getReference('transportTrain'));
    $priceofdelivery->setCountdeliverydays(2);
    $priceofdelivery->setDeliveryprice(55000.00);

    $manager->persist($priceofdelivery);
    $manager->flush();
  }

  public function getOrder()
  {
    return 9;
  }

} 