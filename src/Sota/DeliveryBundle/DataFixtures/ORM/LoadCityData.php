<?php
/**
 *
 */

namespace Sota\DeliveryBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sota\DeliveryBundle\Entity\Delivery\City;


class LoadCityrData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $city = new City();
    $city->setName('Минск');
    $this->addReference('city', $city);
    $manager->persist($city);



    $manager->flush();
  }

  public function getOrder()
  {
    return 2;
  }
}
