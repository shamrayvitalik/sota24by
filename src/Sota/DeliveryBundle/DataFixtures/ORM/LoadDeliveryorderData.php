<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.04.17
 * Time: 16:53
 */

namespace Sota\DeliveryBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sota\DeliveryBundle\Entity\Delivery\Deliveryorder;

class LoadDeliveryorderData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $deliveryorder = new Deliveryorder();
    $deliveryorder->setCity($this->getReference('city'));
    $deliveryorder->setTransport($this->getReference('transportTrain'));
    $deliveryorder->setUser($this->getReference('user'));
    $deliveryorder->setOrderstatus(123456789);
    $deliveryorder->setOrderid('order1234567');
    $deliveryorder->setDateofcreations(new \DateTime("now"));
    $deliveryorder->setCost(2500.0);
    $deliveryorder->setWeight(1000);
    $deliveryorder->setCountofpalets(10);
    $deliveryorder->setOrderstatusId($this->getReference('orderstatus'));

    $manager->persist($deliveryorder);
    $manager->flush();
  }

  public function getOrder()
  {
    return 6;
  }

} 