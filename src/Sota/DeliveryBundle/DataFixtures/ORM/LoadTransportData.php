<?php
/**
 *
 */

namespace Sota\DeliveryBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

;
use Sota\DeliveryBundle\Entity\Delivery\Transport;


class LoadTransportrData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $transport = new Transport();
    $transport->setName('авто');
    $transport->setProductstype($this->getReference('producttype'));
    $this->addReference('transportAuto', $transport);

    $addNewTransport = clone $transport;
    $addNewTransport->setName('train');
    $addNewTransport->setProductstype($this->getReference('producttype'));
    $this->addReference('transportTrain', $addNewTransport);
    $manager->persist($addNewTransport);
    $manager->persist($transport);
    $manager->flush();
  }

  public function getOrder()
  {
    return 4;
  }
}