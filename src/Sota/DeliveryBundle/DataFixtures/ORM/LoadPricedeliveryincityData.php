<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.04.17
 * Time: 17:42
 */

namespace Sota\DeliveryBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sota\DeliveryBundle\Entity\Delivery\Pricedeliveryincity;


class LoadPricedeliveryincityData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $pricedeliveryincity = new Pricedeliveryincity();
    $pricedeliveryincity->setCity($this->getReference('city'));
    $pricedeliveryincity->setProductstype($this->getReference('producttype'));
    $pricedeliveryincity->setPricedeliverycity(10000);
    $pricedeliveryincity->setDiscription('description');

    $manager->persist($pricedeliveryincity);
    $manager->flush();
  }

  public function getOrder()
  {
    return 8;
  }

} 