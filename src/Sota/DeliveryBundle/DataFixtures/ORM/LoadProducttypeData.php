<?php
/**
 *
 */

namespace Sota\DeliveryBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sota\DeliveryBundle\Entity\Delivery\Producttype;


class LoadProducttypeData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $producttype = new Producttype();
    $producttype->setName('пищевые');
    $this->addReference('producttype', $producttype);

    $manager->persist($producttype);
    $manager->flush();
  }

  public function getOrder()
  {
    return 3;
  }
}