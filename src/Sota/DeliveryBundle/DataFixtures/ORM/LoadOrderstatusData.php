<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.04.17
 * Time: 17:09
 */

namespace Sota\DeliveryBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sota\DeliveryBundle\Entity\Delivery\Orderstatus;


class LoadOrderstatusData  extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $orderstatus = new Orderstatus();
    $orderstatus->setName('Готов к отправке');
    $this->addReference('orderstatus', $orderstatus);
    $manager->persist($orderstatus);

    $manager->flush();
  }

  public function getOrder()
  {
    return 5;
  }

} 