<?php
/**
 *
 */

namespace Sota\DeliveryBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sota\DeliveryBundle\Entity\Delivery\Dispatchshedule;


class LoadDispatchsheduleData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $dispatchshedule = new Dispatchshedule();
    $dispatchshedule->setCity($this->getReference('city'));
    $dispatchshedule->setTransport($this->getReference('transportTrain'));
    $dispatchshedule->setDayofdispatche(new \DateTime('now'));
    $dispatchshedule->setDescription('description');

    $manager->persist($dispatchshedule);
    $manager->flush();
  }

  public function getOrder()
  {
    return 7;
  }
} 