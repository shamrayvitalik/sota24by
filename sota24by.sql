-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 29, 2017 at 10:51 AM
-- Server version: 5.7.17-0ubuntu0.16.04.2
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sota24by`
--

-- --------------------------------------------------------

--
-- Table structure for table `delivery_city`
--

CREATE TABLE `delivery_city` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_city`
--

INSERT INTO `delivery_city` (`id`, `name`) VALUES
(1, 'Минск');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_config`
--

CREATE TABLE `delivery_config` (
  `id` int(11) NOT NULL,
  `clef` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_deliveryorder`
--

CREATE TABLE `delivery_deliveryorder` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `transport_id` int(11) NOT NULL,
  `user_iduser` int(11) NOT NULL,
  `orderstatus_id` int(11) NOT NULL,
  `orderid` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `dateofcreations` datetime NOT NULL,
  `cost` double NOT NULL,
  `weight` double NOT NULL,
  `countofpalets` int(11) NOT NULL,
  `orderstatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_deliveryorder`
--

INSERT INTO `delivery_deliveryorder` (`id`, `city_id`, `transport_id`, `user_iduser`, `orderstatus_id`, `orderid`, `dateofcreations`, `cost`, `weight`, `countofpalets`, `orderstatus`) VALUES
(1, 1, 1, 1, 1, 'order1234567', '2017-04-29 10:47:46', 2500, 1000, 10, 123456789);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_dispatchshedule`
--

CREATE TABLE `delivery_dispatchshedule` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `transport_id` int(11) NOT NULL,
  `dayofdispatche` datetime NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_dispatchshedule`
--

INSERT INTO `delivery_dispatchshedule` (`id`, `city_id`, `transport_id`, `dayofdispatche`, `description`) VALUES
(1, 1, 1, '2017-04-29 10:47:46', 'description');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_orderstatus`
--

CREATE TABLE `delivery_orderstatus` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_orderstatus`
--

INSERT INTO `delivery_orderstatus` (`id`, `name`) VALUES
(1, 'Готов к отправке');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_pricedeliveryincity`
--

CREATE TABLE `delivery_pricedeliveryincity` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `productstype_id` int(11) NOT NULL,
  `discription` longtext COLLATE utf8_unicode_ci,
  `pricedeliverycity` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_pricedeliveryincity`
--

INSERT INTO `delivery_pricedeliveryincity` (`id`, `city_id`, `productstype_id`, `discription`, `pricedeliverycity`) VALUES
(1, 1, 1, 'description', 10000);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_priceofdelivery`
--

CREATE TABLE `delivery_priceofdelivery` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `transport_id` int(11) NOT NULL,
  `countdeliverydays` int(11) NOT NULL,
  `deliveryprice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_priceofdelivery`
--

INSERT INTO `delivery_priceofdelivery` (`id`, `city_id`, `transport_id`, `countdeliverydays`, `deliveryprice`) VALUES
(1, 1, 1, 2, 55000);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_producttype`
--

CREATE TABLE `delivery_producttype` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_producttype`
--

INSERT INTO `delivery_producttype` (`id`, `name`, `description`) VALUES
(1, 'пищевые', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_transport`
--

CREATE TABLE `delivery_transport` (
  `id` int(11) NOT NULL,
  `productstype_id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_transport`
--

INSERT INTO `delivery_transport` (`id`, `productstype_id`, `name`) VALUES
(1, 1, 'train'),
(2, 1, 'авто');

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `resident` int(11) NOT NULL,
  `person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `resident`, `person`, `phone`) VALUES
(1, 'testUser', 'testuser', 'testUser@gmail.com', 'testuser@gmail.com', 1, NULL, '$2y$13$Mymie79zK4I62WWlYT.2j.JGsAW9AGZ4uADUcY3dhw9KGhI0DSRju', NULL, NULL, NULL, 'a:0:{}', 2, 'nameOrganization', 951234567);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `delivery_city`
--
ALTER TABLE `delivery_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_config`
--
ALTER TABLE `delivery_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_CA9BD6F2F541EE3A` (`clef`);

--
-- Indexes for table `delivery_deliveryorder`
--
ALTER TABLE `delivery_deliveryorder`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7E626CF96FA75095` (`orderid`),
  ADD KEY `IDX_7E626CF98BAC62AF` (`city_id`),
  ADD KEY `IDX_7E626CF99909C13F` (`transport_id`),
  ADD KEY `IDX_7E626CF93B0E3CD4` (`user_iduser`),
  ADD KEY `IDX_7E626CF9A4CFA76C` (`orderstatus_id`);

--
-- Indexes for table `delivery_dispatchshedule`
--
ALTER TABLE `delivery_dispatchshedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DAAB4CC78BAC62AF` (`city_id`),
  ADD KEY `IDX_DAAB4CC79909C13F` (`transport_id`);

--
-- Indexes for table `delivery_orderstatus`
--
ALTER TABLE `delivery_orderstatus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_4EBB5C3B5E237E06` (`name`);

--
-- Indexes for table `delivery_pricedeliveryincity`
--
ALTER TABLE `delivery_pricedeliveryincity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_370249D28BAC62AF` (`city_id`),
  ADD KEY `IDX_370249D2BA4CE03A` (`productstype_id`);

--
-- Indexes for table `delivery_priceofdelivery`
--
ALTER TABLE `delivery_priceofdelivery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C6574E108BAC62AF` (`city_id`),
  ADD KEY `IDX_C6574E109909C13F` (`transport_id`);

--
-- Indexes for table `delivery_producttype`
--
ALTER TABLE `delivery_producttype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_transport`
--
ALTER TABLE `delivery_transport`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_22CE6DBCBA4CE03A` (`productstype_id`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `delivery_city`
--
ALTER TABLE `delivery_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `delivery_config`
--
ALTER TABLE `delivery_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `delivery_deliveryorder`
--
ALTER TABLE `delivery_deliveryorder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `delivery_dispatchshedule`
--
ALTER TABLE `delivery_dispatchshedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `delivery_orderstatus`
--
ALTER TABLE `delivery_orderstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `delivery_pricedeliveryincity`
--
ALTER TABLE `delivery_pricedeliveryincity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `delivery_priceofdelivery`
--
ALTER TABLE `delivery_priceofdelivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `delivery_producttype`
--
ALTER TABLE `delivery_producttype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `delivery_transport`
--
ALTER TABLE `delivery_transport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `delivery_deliveryorder`
--
ALTER TABLE `delivery_deliveryorder`
  ADD CONSTRAINT `FK_7E626CF93B0E3CD4` FOREIGN KEY (`user_iduser`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_7E626CF98BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `delivery_city` (`id`),
  ADD CONSTRAINT `FK_7E626CF99909C13F` FOREIGN KEY (`transport_id`) REFERENCES `delivery_transport` (`id`),
  ADD CONSTRAINT `FK_7E626CF9A4CFA76C` FOREIGN KEY (`orderstatus_id`) REFERENCES `delivery_orderstatus` (`id`);

--
-- Constraints for table `delivery_dispatchshedule`
--
ALTER TABLE `delivery_dispatchshedule`
  ADD CONSTRAINT `FK_DAAB4CC78BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `delivery_city` (`id`),
  ADD CONSTRAINT `FK_DAAB4CC79909C13F` FOREIGN KEY (`transport_id`) REFERENCES `delivery_transport` (`id`);

--
-- Constraints for table `delivery_pricedeliveryincity`
--
ALTER TABLE `delivery_pricedeliveryincity`
  ADD CONSTRAINT `FK_370249D28BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `delivery_city` (`id`),
  ADD CONSTRAINT `FK_370249D2BA4CE03A` FOREIGN KEY (`productstype_id`) REFERENCES `delivery_producttype` (`id`);

--
-- Constraints for table `delivery_priceofdelivery`
--
ALTER TABLE `delivery_priceofdelivery`
  ADD CONSTRAINT `FK_C6574E108BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `delivery_city` (`id`),
  ADD CONSTRAINT `FK_C6574E109909C13F` FOREIGN KEY (`transport_id`) REFERENCES `delivery_transport` (`id`);

--
-- Constraints for table `delivery_transport`
--
ALTER TABLE `delivery_transport`
  ADD CONSTRAINT `FK_22CE6DBCBA4CE03A` FOREIGN KEY (`productstype_id`) REFERENCES `delivery_producttype` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
